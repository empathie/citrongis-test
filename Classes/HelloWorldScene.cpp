#include "HelloWorldScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();
    layer->scheduleUpdate();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

void HelloWorld::update(float delta)
{
	debugLabel->setString("updated");
	debugLabel->setPosition(Vec2(origin.x + debugLabel->getContentSize().width/2,
            origin.y + debugLabel->getContentSize().height/2));
    HelloWorld::MenuMoveRight();
    HelloWorld::MenuMoveLeft();
    HelloWorld::MenuMoveTop();
    HelloWorld::MenuMoveBottom();
}

void HelloWorld::draw(Renderer *renderer, const Mat4& transform, uint32_t flags)
{
	if (SpriteList.size() < 2)
		return;

	int i = 0;
	Sprite *firstSprite = NULL;
	Sprite *secondSprite = NULL;

    ccDrawColor4F(1.0f, 0.0f, 0.0f, 1.0f);

    while (i <= (SpriteList.size() - 2))
	{
		firstSprite = SpriteList[i];
		secondSprite = SpriteList[i + 1];
	    ccDrawLine(firstSprite->getPosition(), secondSprite->getPosition());
	    ++i;
	}
    ccDrawLine(SpriteList[0]->getPosition(), SpriteList[SpriteList.size() - 1]->getPosition());
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    //	Setting variables values

    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
//    SpriteList = new std::vector<Sprite *>();

    //	Creating Leave Button
    closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));


	//	Button Labels creation (top, right, bot, left)
	auto	rightLabel = LabelTTF::create("Right", "Arial", 24);
	rightItem = MenuItemLabel::create(rightLabel);

	rightItem->setPosition(Vec2(origin.x + visibleSize.width - visibleSize.width / 4 - closeItem->getContentSize().width/2 ,
                                origin.y + visibleSize.height / 2 - closeItem->getContentSize().height/2));

	auto	leftLabel = LabelTTF::create("Left", "Arial", 24);
	leftItem = MenuItemLabel::create(leftLabel);

	leftItem->setPosition(Vec2(origin.x + visibleSize.width / 4 - closeItem->getContentSize().width/2 ,
                                origin.y + visibleSize.height / 2 - closeItem->getContentSize().height/2));

	auto	topLabel = LabelTTF::create("Top", "Arial", 24);
	topItem = MenuItemLabel::create(topLabel);

	topItem->setPosition(Vec2(origin.x + visibleSize.width / 2 - closeItem->getContentSize().width/2 ,
			origin.y + visibleSize.height - closeItem->getContentSize().height/2));

	auto	bottomLabel = LabelTTF::create("Bottom", "Arial", 24);
	bottomItem = MenuItemLabel::create(bottomLabel);

	bottomItem->setPosition(Vec2(origin.x + visibleSize.width / 2 - closeItem->getContentSize().width/2 ,
            origin.y + closeItem->getContentSize().height/2));


	//	Adding buttons to 2 menus to get multi-touch.
	menu = Menu::create(closeItem, rightItem, bottomItem, leftItem, topItem, nullptr);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
	menuHeight = Menu::create(closeItem, rightItem, bottomItem, leftItem, topItem, nullptr);
    menuHeight->setPosition(Vec2::ZERO);
    this->addChild(menuHeight, 1);


    //	Creating the label which will be moved
    label = LabelTTF::create("Ceci est un test", "Arial", 24);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height / 2 - label->getContentSize().height / 2));

    //	DebugLabel for debugging purpose only.
    debugLabel = LabelTTF::create("Nothing", "Arial", 24);


    this->addChild(label, 1);
    this->addChild(debugLabel, 1);

    //	Sets event listeners
    addEvents();
    return true;
}

void HelloWorld::MenuMoveRight()
{
	if (!rightItem->isSelected())
		return ;
	Vec2	position = label->getPosition();

	debugLabel->setString(std::to_string(label->getPosition().x) + std::string(" ") + std::to_string(label->getPosition().y));
	debugLabel->setPosition(Vec2(origin.x + debugLabel->getContentSize().width/2,
            origin.y + debugLabel->getContentSize().height/2));
	if (position.x < (visibleSize.width)) {
		label->setPosition(Vec2(position.x + 5, position.y));
	}
}

void HelloWorld::MenuMoveLeft()
{
	if (!leftItem->isSelected())
		return ;
	Vec2	position = label->getPosition();

	debugLabel->setString(std::to_string(label->getPosition().x) + std::string(" ") + std::to_string(label->getPosition().y));
	debugLabel->setPosition(Vec2(origin.x + debugLabel->getContentSize().width/2,
            origin.y + debugLabel->getContentSize().height/2));
	if (position.x > 0) {
		label->setPosition(Vec2(position.x - 5, position.y));
	}
}

void HelloWorld::MenuMoveTop()
{
	if (!topItem->isSelected())
		return ;
	Vec2	position = label->getPosition();

	debugLabel->setString(std::to_string(label->getPosition().x) + std::string(" ") + std::to_string(label->getPosition().y));
	debugLabel->setPosition(Vec2(origin.x + debugLabel->getContentSize().width/2,
            origin.y + debugLabel->getContentSize().height/2));
	if (position.y < (visibleSize.height)) {
		label->setPosition(Vec2(position.x, position.y + 5));
	}
}

void HelloWorld::MenuMoveBottom()
{
	if (!bottomItem->isSelected())
		return ;
	Vec2	position = label->getPosition();

	debugLabel->setString(std::to_string(label->getPosition().x) + std::string(" ") + std::to_string(label->getPosition().y));
	debugLabel->setPosition(Vec2(origin.x + debugLabel->getContentSize().width/2,
            origin.y + debugLabel->getContentSize().height/2));
	if (position.y > 0) {
		label->setPosition(Vec2(position.x, position.y - 5));
	}
}

void HelloWorld::addEvents()
{
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);

    listener->onTouchBegan = [&](Touch* touch, Event* event)
    {
        Vec2 p = touch->getLocation();
        Rect rect = this->getBoundingBox();

        if(rect.containsPoint(p))
        {
            return true; // to indicate that we have consumed it.
        }

        return false; // we did not consume this event, pass thru.
    };

    listener->onTouchEnded = [=](Touch* touch, Event* event)
    {
        HelloWorld::touchEvent(touch, touch->getLocation());
    };

    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}

void HelloWorld::touchEvent(Touch* touch, Vec2 point)
{
    auto blankRectangle = Sprite::create();
    blankRectangle->setColor(Color3B::GREEN);
    blankRectangle->setPosition(point);
    blankRectangle->setTextureRect(Rect(0, 0, 10, 10));
    blankRectangle->setOpacity(180);
    SpriteList.push_back(blankRectangle);
    this->addChild(blankRectangle, 2);
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
