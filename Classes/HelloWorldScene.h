#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include <vector>

USING_NS_CC;

class HelloWorld : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    //	a selector callback - move the text to the right
    void MenuMoveRight();

    //	a selector callback - move the text to the Left
    void MenuMoveLeft();

    //	a selector callback - move the text to the Top
    void MenuMoveTop();

    //	a selector callback - move the text to the Bottom
    void MenuMoveBottom();

    //	Implements update from Node to move label
    virtual void update(float delta);

    //	Add listener events
    void addEvents();

    //	Callback when touched
    void touchEvent(Touch* touch, Vec2 point);

    //	Draw method
    virtual void draw(Renderer *renderer, const Mat4& transform, uint32_t flags);


    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

private:
    Size visibleSize;
    Vec2 origin;

    //	Debug label text
    LabelTTF *debugLabel;

    //	Hello world text
    LabelTTF *label;

    Menu *menu;
    Menu *menuHeight;

    //	Menu items
    MenuItemImage* closeItem;

    //	To move the text
    MenuItemLabel *rightItem;
    MenuItemLabel *leftItem;
    MenuItemLabel *topItem;
    MenuItemLabel *bottomItem;

    //	SpriteList
    std::vector<Sprite *>	SpriteList;
};

#endif // __HELLOWORLD_SCENE_H__
